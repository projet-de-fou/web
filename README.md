# client-web

> Partie Front du Projet de Fin d'année

- Utilisation de vue.js
- Utilisation de Materialize.css
- Utilisation de vue-router
- Utilisation de vue-cookie

> Pour lancer l'application

- Télécharger les sources
- Lancer la commande à la racine du projet : npm install
- Configuer l'URL dans : main.js (http://rochdion.net -> http://localhost)
- Puis npm run dev

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
