import Vue from 'vue'
import Router from 'vue-router'

import Connect from '@/components/Connect'
import Home from '@/components/Home'
import About from '@/components/About'
import Contact from '@/components/Contact'
import Signup from '@/components/Signup'
import PlayScreen from '@/components/PlayScreen'
import Score from '@/components/Score'
import Profile from '@/components/Profile'

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Connect',
      component: Connect
    },
    {
      path: '/Connect',
      name: 'Connect',
      component: Connect
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    },
    {
      path: '/Sign',
      name: 'Sign',
      component: Signup
    },
    {
      path: '/PlayScreen',
      name: 'PlayScreen',
      component: PlayScreen
    },
    {
      path: '/Score',
      name: 'Score',
      component: Score
    },
    {
      path: '/Profile',
      name: 'Profile',
      component: Profile
    }
  ]
})
