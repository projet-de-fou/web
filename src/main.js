// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ProgressBar from 'vuejs-progress-bar'
import 'materialize-social/materialize-social.css'
import splitPane from 'vue-splitpane'
import axios from 'axios';
import VueAxios from 'vue-axios'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.use(ProgressBar);
Vue.use(VueAxios, axios);
Vue.component('split-pane', splitPane);
Vue.use(ProgressBar);
// register modal component
Vue.component('modal', {
    template: '#modal-template'
});
let VueCookie = require('vue-cookie');
Vue.use(VueCookie);


export const roomStore = new Vue({
  data: {
    name: '',
    roomCode: '',
    socket: '',
    hostClientId: '',
    player: [],
    theme: [],
    url: 'http://rochdion.net',
    score: ''
  }
});

/* eslint-disable no-new */
new Vue({
    el: '#app',
    data: {
        showModal: false
    },
    router,
    template: '<App/>',
    components: { App }
});
