#!/usr/bin/env bash
cd ~/web;
pkill -f npm;
pkill -f node;
git fetch origin;
git checkout testserver;
git merge origin/testserver;
npm install;
npm run dev >> ~/web.log 2>&1 &

# also reload server, just in case
cd ~/serveur;
npm install;
node app.js >> ~/server.log 2>&1 &
